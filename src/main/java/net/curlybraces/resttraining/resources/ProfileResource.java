package net.curlybraces.resttraining.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.curlybraces.resttraining.model.Profile;
import net.curlybraces.resttraining.service.ProfileService;

@Path("/profiles")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class ProfileResource {

	ProfileService profileService = ProfileService.getInstance();

	@GET
	public List<Profile> getProfiles() {
		return this.profileService.getAllProfiles();
	}

	@Path("/{profileName}")
	@GET
	public Profile getProfile(@PathParam("profileName") String name) {
		return profileService.getProfile(name);
	}

	@POST
	public Profile addProfile(Profile profile) {
		profile.setId((long) (Math.random() * 100));
		profileService.addProfile(profile);
		return profile;
	}

	@PUT
	@Path("/{profileName}")
	public Profile updateProfile(@PathParam("profileName") String name, Profile profile) {
		profile.setProfileName(name);
		return profileService.updateProfile(profile);
	}

	@DELETE
	@Path("/{profileName}")
	public void deleteProfile(@PathParam("profileName") String name) {
		profileService.removeProfile(name);
	}

}
