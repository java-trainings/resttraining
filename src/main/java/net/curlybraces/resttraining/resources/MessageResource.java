package net.curlybraces.resttraining.resources;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.curlybraces.resttraining.model.Message;
import net.curlybraces.resttraining.service.MessageService;

@Path(value = "/messages")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
//@Path("/messages")
public class MessageResource {

	MessageService messageService = MessageService.getInstance();
	
	@GET
	//@Produces(MediaType.APPLICATION_JSON)
	public List<Message> getMessages() {
		return this.messageService.getAllMessages();
	}
	
	@Path("/{messageId}") 
	@GET
	//@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Message getMessage(@PathParam("messageId") long id) {
		return messageService.getMessage(id);
	}
	
	@POST
	//@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	//@Produces(MediaType.APPLICATION_JSON)
	public Message addMessage(Message message) {
		message.setId((long) (Math.random() * 100));
		message.setCreated(new Date());
		messageService.addMessage(message);
		return message;
	}
	
	@PUT
	@Path("/{messageId}")
	//@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	//@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Message updateMessage(@PathParam("messageId") long id, Message message) {
		message.setId(id);
		return messageService.updateMessage(message);
	}
	
	@DELETE
	@Path("/{messageId}")
	//@Consumes({MediaType.APPLICATION_JSON})
	//@Produces(MediaType.APPLICATION_JSON)
	public void deleteMessage(@PathParam("messageId") long id) {
		messageService.removeMessage(id);
	}

}
