package net.curlybraces.resttraining.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="msg")
public class Message {

	private long id;
	private String message;
	private Date created;
	private String author;
	
	// no-arg constructor
	// needed when dealing with xml/json conversions
	public Message() {}
	
	public Message(String message, String author) {
		this.id = (long)(Math.random() * 100);
		this.created = new Date();
		this.message = message;
		this.author = author;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
}
