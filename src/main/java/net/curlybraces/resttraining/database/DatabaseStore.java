package net.curlybraces.resttraining.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.curlybraces.resttraining.model.Message;
import net.curlybraces.resttraining.model.Profile;

public class DatabaseStore {

	private static List<Profile> profiles = new ArrayList<Profile>(
			Arrays.asList(new Profile("mhamidi", "Mohamed", "Hamidi"), new Profile("jdoe", "John", "Doe")));
	private static List<Message> messages = new ArrayList<Message>(
			Arrays.asList(new Message("msg1", "mhamidi"), new Message("msg2", "mhamidi")));

	public static List<Profile> getProfiles() {
		return profiles;
	}

	public static List<Message> getMessages() {
		return messages;
	}
	
}
