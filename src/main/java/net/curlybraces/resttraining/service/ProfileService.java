package net.curlybraces.resttraining.service;

import java.util.List;
import java.util.Optional;

import net.curlybraces.resttraining.database.DatabaseStore;
import net.curlybraces.resttraining.model.Profile;

public class ProfileService {

	public static ProfileService instance;

	public static ProfileService getInstance() {
		if (instance == null) {
			instance = new ProfileService();
		}
		return instance;
	}

	public Profile getProfile(String profileName) {
		Optional<Profile> optionalProfile = DatabaseStore.getProfiles().stream()
				.filter(profile -> profile.getProfileName().equals(profileName))
				.findFirst();
		return optionalProfile.orElse(null);
	}

	public Profile addProfile(Profile profile) {
		DatabaseStore.getProfiles().add(profile);
		return profile;
	}

	public Profile updateProfile(Profile profile) {
		Optional<Profile> optionalProfile = DatabaseStore.getProfiles().stream()
				.filter(prfl -> prfl.getProfileName().equals(profile.getProfileName())).findFirst();
		if (!optionalProfile.isPresent()) {
			return null;
		}

		DatabaseStore.getProfiles().set(DatabaseStore.getProfiles().indexOf(optionalProfile.get()), profile);
		return profile;
	}

	public Profile removeProfile(String profileName) {
		Optional<Profile> optionalProfile = DatabaseStore.getProfiles().stream().filter(profile -> profile.getProfileName().equals(profileName))
				.findFirst();
		if (optionalProfile.isPresent()) {
			Profile profile = optionalProfile.get();
			DatabaseStore.getProfiles().remove(profile);
			return profile;
		}
		return null;
	}

	public List<Profile> getAllProfiles() {
		return DatabaseStore.getProfiles();
	}

}
