package net.curlybraces.resttraining.service;

import java.util.List;
import java.util.Optional;

import net.curlybraces.resttraining.database.DatabaseStore;
import net.curlybraces.resttraining.model.Message;

public class MessageService {

	public static MessageService instance;
	
	public static MessageService getInstance() {
		if(instance == null) {
			instance = new MessageService();
		}
		return instance;
	}
	
	public Message getMessage(long id) {
		Optional<Message> optionalMessage = DatabaseStore.getMessages().stream().filter(msg -> msg.getId() == id).findFirst();
		return optionalMessage.orElse(null);
	}
	
	public Message addMessage(Message message) {
		DatabaseStore.getMessages().add(message);
		return message;
	}
	
	public Message updateMessage(Message message) {
		Optional<Message> optionalMessage = DatabaseStore.getMessages().stream().filter(msg -> msg.getId() == message.getId()).findFirst();
		if(!optionalMessage.isPresent()) {			
			return null;
		}
		
		DatabaseStore.getMessages().set(DatabaseStore.getMessages().indexOf(optionalMessage.get()), message);
		return message;
	}
	
	public Message removeMessage(long id) {
		Optional<Message> optionalMessage = DatabaseStore.getMessages().stream().filter(msg -> msg.getId() == id).findFirst();
		if(optionalMessage.isPresent()) {
			Message message = optionalMessage.get();
			DatabaseStore.getMessages().remove(message);
			return message;
		}
		return null;
	}

	
	public List<Message> getAllMessages() {
		return DatabaseStore.getMessages();
	}
}
